// // $ sau jQuery

// // HIDE
function hideElement() {
  $(".search-area button").hide();
}

// // SHOW
function showElement() {
  $(".search-area button").show();
}

// // ADD/REMOVE/TOGGLE CLASS
function classElement() {
  $(".search-area input").click(function() {
    $('.navigation-bar').addClass('xxxx');
    $('.navigation-bar').removeClass('xxxx');
    $('.navigation-bar').toggleClass('xxxx');
  });
}

// // CLICk
function clickFunction() {
  $(".burger-menu").click(function() {
    console.log('test');
    $("nav ul").toggleClass('opened');
    // $("nav ul").show();
  });
}

function clickFunction2() {
  $(".product-list li").click(function() {
    $(".product-list article").removeClass("selected");
    $(this).find('article').addClass('selected');
    // $(this).parent().parent().addClass('yyy');
    $(this).closest('section').addClass('zzzz');
  });
}

function clickFunction3() {
  $(".logo").click(function() {
    $(this).closest(".navigation-bar").siblings("main").find("section").addClass('super');
  });
}

function clickFunction4() {
  $("main section").click(function() {
    console.log($(this).attr("class"));
  });
}

function clickFunction5() {
  $(".logo").click(function(){
    $('header').hide();
    $('header').fadeOut(3000);
  });
}

function clickFunction6() {
  $(".prev-btn").click(function() {
    $('header').fadeIn();
  })
}

function checkDevice() {
  var x = "test";
  if ($(window).width() < 760) {
    console.log("ce vrei tu");
  } else {
    console.log("altceva")
  }
}

function cloneElement() {
  var newHeader = $('header').clone();
  $('body').prepend(newHeader);

  var newCarouselPosition = $('#carousel').detach();
  $('main').append(newCarouselPosition);
}

function itemDimensions() {
  $('.product-list article').click(function() {
    console.log('width', $(this).outerWidth(true));
    console.log('height', $(this).outerHeight(true));
  });
}

function navigationHeight() {
  console.log('Height simplu', $('.navigation-bar').height());
  console.log('Height simplu + padding',$('.navigation-bar').innerHeight());
  console.log('Height simplu + padding + border',$('.navigation-bar').outerHeight());
  console.log('Height simplu + padding + border + margin',$('.navigation-bar').outerHeight(true));
}

function navAnimation() {
  $('.navigation-bar').click(function() {
    // animatiile se fac pe rand
    $(this).animate({
      "border-bottom-width": '16px'
    }, 2000, function() {
      $(this).animate({
        "margin-bottom": '40px'
      }, 1000)
    })

    // animatiile se fac simultan
    // $(this).animate({
    // 	"border-bottom-width": '16px',
    // 	"margin-bottom": '40px'
    // }, 2000)
  });
}

$(document).ready(function() {
  console.log("ready");
  cloneElement();
  itemDimensions();
  navigationHeight();
  navAnimation();
  // $('.product-list').remove();
  // $('.product-list').empty();
  var noItems = $('.product-list').children().length;

  $('.product-list').before(noItems);
  // checkDevice();
  // html/css
});

// $(window).load(function() {
// 	//aduce in plus IMG
// });

// $(window).resize(function() {
// 	// console.log("resize", $(window).width());
// 	checkDevice();
// });

