function hpCarousel() {
  if ($(window).width() > 768) {
    console.log('viewport mare', $(window).width());
    if(!$('.js-carousel').hasClass('slick-initialized')) {
      $('.js-carousel').slick();
    }
  } else {
    console.log('viewport mic', $(window).width());
    $('.js-carousel').slick('unslick');
  }
}

$(document).ready(function(){
  hpCarousel();
});

$(window).resize(function(){
  hpCarousel();
});