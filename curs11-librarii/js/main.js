function toggleMenu() {
	$('.menu-item').click(function() {
		$('.submenu').removeClass('show');
		$(this).siblings('.submenu').toggleClass('show');
	});
}

function toogleMiniSearch() {
	$('.search-icon').click(function() {
		$(this).siblings('.search-input').toggleClass('show');
	});
}

function carousel() {
	$('.carousel').slick({
		autoplay: true,
		dots: true,
		arrows: false,
		slidesToShow: 2,
		responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1
		      }
		    }
		]
	});
}

function imagePopup() {
	$('.test-popup-link').magnificPopup({
		type: 'image',
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
	  // other options
	});
}

$(document).ready(function() {
	toggleMenu();
	toogleMiniSearch();
	carousel();
	imagePopup();
});