function validare() {
	var lastNameRequired = $("#lastName").attr("data-required");
	var phoneRequired = $("#phone").attr("data-required");
	var phoneValid = $("#phone").attr("data-valid");
	var phoneLength = $("#phone").attr("data-length");

	$('#contact-form').validate({
		rules: {
			lastName: {
				required: true,
				minlength: 3
			},

			firstName: {
				required: true,
				minlength: 3,
				maxlength: 10
			},

			phone: {
				required: true,
				number: true,
				minlength: 10,
				maxlength: 10
			},

			email: {
				required: true,
				email: true
			},

			files: {
				required: true,
				extension: 'jpg|doc'
			},

			password: {
				required: true,
				minlength: 4
			},

			confirmPassword: {
				required: true,
				equalTo: "#password"
			}
		},

		messages: {
			lastName: {
				required: lastNameRequired
			},

			firstName: {
				required:"prenumele este obligatoriu",
				minlength:"prenumele trebuie sa aiba mai mult de trei caractere",
				maxlength:"prenumele trebuie sa aiba maxim zece caractere"
			},

			phone: {
				required: phoneRequired,
				number: phoneValid,
				minlength: phoneLength
			},

			email: {
				required: "fiels is requierd",
				email: "not a valid email address"
			},

			files: {
				required: "CV is required",
				extension: "CV must be jpg file"
			},

			password: {
				required: 'Parola obligatorie',
				minlength: 'Parola trebuie sa contina minim 4 caractere'
			},
			confirmPassword: {
				required: "camp obligatoriu",
				equalTo: "parola trebuie sa coincida"
			}
		}
	}) 
}

$(document).ready( function() {
	validare();
})